function set_conda {
    HOME=$(echo ~)
    INSTALL_PATH=$HOME"/Exec"
    MINICONDA_PATH=$INSTALL_PATH"/miniconda3/bin"
    PYTHON_PATH=$(which python)
    SCRIPT="Miniconda3-latest-Linux-x86_64.sh"
    REQUIREMENTS="jupyter numpy pandas"
    DL_PATH=$HOME"/Sources"
    DL_LINK="https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"

    if echo $PYTHON_PATH | grep -q $INSTALL_PATH
    then
        echo "Good python version"
    else
        cd $DL_PATH
	if [ ! -f $SCRIPT ]
	then
	    curl -LO $DL_LINK
	fi
	if [ ! -d $MINICONDA_PATH ]
	then
	    sh $SCRIPT -b -p $INSTALL_PATH"/miniconda3"
	fi
	conda install -y $(echo $REQUIREMENTS)
	clear
	echo "Which python:"
	if grep -q "^export PATH=$MINICONDA_PATH" ~/.bashrc
	then
	    echo "export already in .bashrc"
	else
	    echo "adding export to .bashrc"
	    echo "export PATH=$MINICONDA_PATH" >> ~/.bashrc
	fi
	source ~/.bashrc
    fi
}
