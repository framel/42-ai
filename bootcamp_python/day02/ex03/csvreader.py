class CsvReader():
    def __init__(self, filename, sep=',', header=False, skip_top=0, skip_bottom=0):
        self.filename = filename
        self.sep = sep
        if header:
            self.header = 1
        else:
            self.header = 0
        self.skip_top = skip_top
        self.skip_bottom = skip_bottom

    def __enter__(self):
        self.file = open(self.filename, 'r')
        self.content = self.file.read()
        self.content = self.content.splitlines()
        while self.skip_top:
            self.content.pop(0)
            self.skip_top -= 1
        while self.skip_bottom:
            self.content.pop(-1)
            self.skip_bottom -= 1
        length = len(self.content[0].split(self.sep))
        for line in self.content:
            if len(line.split(self.sep)) != length:
                return None
            for word in line.split(self.sep):
                if len(word) == 0:
                    return None
        return self

    def __exit__(self, type, value, traceback):
        self.file.close()

    def getdata(self):
        data = []
        for line in self.content[self.header::]:
             data.append(line.split(self.sep))
        return data

    def getheader(self):
        if self.header:
            return self.content[0].split(self.sep)
