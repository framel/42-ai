from time import sleep
from time import time

def progressbar(lst):
    start = time()
    lenght = len(lst)
    now = 0
    bar = ">"
    bar_size = 20
    for elem in lst:
        etime = time() - start
        now += 1
        frac = now/lenght
        percent = int(frac*100)
        eta = etime/frac
        size = int(now*bar_size/lenght)
        bar = bar.rjust(size, '=')
        bar2 = bar.ljust(bar_size)
        print("ETA: {:1.2f}s [{:3d}%][{}] {}/{} | elapsed time {:1.2f}s"
              .format(eta,percent,bar2,now,lenght,etime), end='\r')
        yield(elem)
