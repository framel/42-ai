#! /bin/sh

if [ $# = 0 ]
then
    mkdir 'ai42'
    mkdir 'ai42/logging'
    touch 'ai42/__init__.py'
    touch 'ai42/logging/__init__.py'
    cp 'progressbar.py' 'ai42'
    cp 'log.py' 'ai42/logging'
    python setup.py sdist bdist_wheel
else
    if [ $1 = 'clean' ] || [ $1 = 'fclean' ]
    then
	rm -fr 'ai42'
    fi
    if [ $1 = 'fclean' ]
    then
	rm -rf 'build'
	rm -rf 'dist'
	rm -rf 'ai42.egg-info'
    fi
    if [ $1 = 'install' ]
    then
	pip install ./dist/ai42-1.0.0.tar.gz
    fi
    if [ $1 = 'uninstall' ]
    then
	pip uninstall ai42
    fi
fi
