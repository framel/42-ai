from time import time
from getpass import getuser

def log(func):
    def logger(*args):
        machine = open("machine.log", "a+")
        start = time()
        ret = func(*args)
        exec_time = time() - start
        if exec_time >= 1:
            machine.write(
                "({})Running: {}\t\t[ exec-time = {:.3f} s  ]\n"
                .format(getuser(),
                        func.__name__.replace('_', ' ').title(),
                        exec_time))
        else:
            machine.write(
                "({})Running: {}\t\t[ exec-time = {:.3f} ms ]\n"
                .format(getuser(),
                        func.__name__.replace('_', ' ').title(),
                        exec_time * 1000))
            machine.close()
        return ret
    return logger
