from FileLoader import FileLoader

def youngestFellah(df, year):
    f_filter = data['Sex'] == 'F'
    m_filter = data['Sex'] == 'M'
    y_filter = data['Year'] == year
    out = df.loc[:,['Sex', 'Age', 'Year']]
    out = out.where(y_filter)
    out = out.loc[:,['Sex', 'Age']]
    ret = {'f':out.where(f_filter).min().to_dict().get('Age'),
           'm':out.where(m_filter).min().to_dict().get('Age')}
    return ret
