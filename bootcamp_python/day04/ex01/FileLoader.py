import pandas as pd

class FileLoader:
    @staticmethod
    def load(path):
        data = pd.read_csv(path)
        print("Loading dataset of dimensions {} x {}".format(*data.shape))
        return data

    @staticmethod
    def display(df, n):
        if n < 0:
            print(df.iloc[n:,:])
        else:
            print(df.iloc[:n,:])
