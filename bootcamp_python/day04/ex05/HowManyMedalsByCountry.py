import pandas as pd

def howManyMedalsByCountry(df, country):
    n_filter = df.get('Team') == country
    out = df.where(n_filter)
    out = out.dropna()
    medal = {'Gold':'G','Silver':'S','Bronze':'B'}
    out = out.replace({"Medal":medal})
    out = out.loc[:,['Year', 'Medal']]
    out.Year = out.Year.astype('int32')
    out = out.fillna(False)
    out = out.sort_values(by="Year")
    ret = {}
    for i, line in out.iterrows():
        if line['Year'] not in ret.keys():
            ret[line['Year']] = {'G': 0, 'S': 0, 'B': 0}
        if line['Medal']:
            ret[line['Year']][line['Medal']] += 1
    return ret
