import pandas as pd

class SpatioTemporalData:
    def __init__(self, df):
        self.df = df

    def when(self, location):
        out = self.df.loc[self.df.City == location, ['Year']]
        out = out.dropna()
        out = out.drop_duplicates()
        ret = []
        for i, line in out.iterrows():
            ret.append(line['Year'])
        return ret

    def where(self, date):
        out = self.df.loc[self.df.Year == date, 'City']
        out = out.unique()
        return out
