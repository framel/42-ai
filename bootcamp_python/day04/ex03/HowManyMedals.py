import pandas as pd

def howManyMedals(df, name):
    n_filter = df.get('Name') == name
    out = df.where(n_filter)
    out = out.dropna(subset=['Name'])
    medal = {'Gold':'G','Silver':'S','Bronze':'B'}
    out = out.replace({"Medal":medal})
    out = out.loc[:,['Year', 'Medal']]
    out.Year = out.Year.astype('int32')
    out = out.fillna(False)
    ret = {}
    for i, line in out.iterrows():
        if line['Year'] not in ret.keys():
            ret[line['Year']] = {'G': 0, 'S': 0, 'B': 0}
        if line['Medal']:
            ret[line['Year']][line['Medal']] += 1
    return ret
