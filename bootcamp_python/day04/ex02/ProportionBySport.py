from FileLoader import FileLoader

def proportionBySport(df, year, sport, gender):
    y_filter = df["Year"] == year
    s_filter = df["Sport"] == sport
    g_filter = df["Sex"] == gender
    out = df.loc[:,['Name', 'Year','Sport','Sex']]
    out = out.where(y_filter & s_filter & g_filter)
    out = out.dropna()
    out = out.drop_duplicates()
    out = out.shape[0]
    total = df.loc[:,['Name', 'Year','Sex']]
    total = total.where(y_filter & g_filter)
    total = total.drop_duplicates()
    total = total.dropna()
    total = total.shape[0]
    return out/total

