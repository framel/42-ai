import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

class Komparator:
    def __init__(self, df):
        self.df = df

    def compare_box_plots(self, categorical_var, numerical_var):
        sns.boxplot(x=numerical_var, y=categorical_var, data=self.df)
        plt.show()

    def density(self, categorical_var, numerical_var):
        for i, elem in enumerate(self.df[categorical_var].drop_duplicates()):
            sns.kdeplot(self.df[numerical_var][self.df[categorical_var] == elem].dropna(), label=elem)
        plt.show()
        
    def compare_histograms(self, categorical_var, numerical_var):
        out = self.df[categorical_var].dropna().drop_duplicates()
        nb = len(out)
        fig, ax = plt.subplots(1, nb + 1)
        for i, elem in enumerate(out):
            sns.distplot(self.df[numerical_var][self.df[categorical_var] == elem].dropna(), \
                         kde=False, ax=ax[i], hist=True)
            ax[i].set_title("{}: {}".format(numerical_var, elem))
        for elem in out:
            sns.distplot(self.df[numerical_var][self.df[categorical_var] == elem].dropna(), \
                         kde=False, ax=ax[nb], hist=True)
            ax[nb].set_title("{}: {}".format(numerical_var, out.values))
        plt.show()
