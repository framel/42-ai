import pandas as pd
import matplotlib.pyplot as plt

class MyPlotLib:
    @staticmethod
    def histogram(data, features):
        data[features].dropna().hist()
        plt.show()

    @staticmethod
    def density(data, features):
        data[features].dropna().plot.kde()
        plt.show()

    @staticmethod
    def pair_plot(data, features):
        pd.plotting.scatter_matrix(data[features])
        plt.show()

    @staticmethod
    def box_plot(data, features):
        data[features].plot.box()
        plt.show()

