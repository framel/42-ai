import numpy as np

class ColorFilter:
    @staticmethod
    def invert(array):
        return 1 - array

    @staticmethod
    def to_blue(array):
        blue_arr = np.zeros(array.shape)
        blue_arr[:,:,2:3] = array[:,:,2:3]
        return blue_arr

    @staticmethod
    def to_green(array):
        green_arr = np.copy(array)
        green_arr[:,:,0:1] = green_arr[:,:,0:1] * 0
        green_arr[:,:,2:3] = green_arr[:,:,2:3] * 0
        return green_arr

    @staticmethod
    def to_red(array):
        blue_arr = ColorFilter.to_blue(array)
        green_arr = ColorFilter.to_green(array)
        return array - (blue_arr + green_arr)

    @staticmethod
    def celluloid(array, thresh=4):
        thr = np.arange(0, 1.1, 1 / thresh)
        val = np.linspace(0, 1, num=thresh)
        i = 0
        while i < thresh:
            print(thr[i], val[i])
            array[(thr[i] < array) & (array < thr[i + 1])] = val[i]
            i += 1
        return array
 
    @staticmethod
    def to_grayscale(array, filter='w'):
        if filter == 'm' or filter == 'mean':
            gray_arr = np.sum(array, axis=2)
            gray_arr = gray_arr / 3
            return gray_arr
        else:
            array[:,:,0:1] = 0.299 * array[:,:,0:1]
            array[:,:,1:2] = 0.587 * array[:,:,1:2]
            array[:,:,2:3] = 0.114 * array[:,:,2:3]
            return np.sum(array, axis=2)
