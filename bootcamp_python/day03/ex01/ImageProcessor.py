import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np

class ImageProcessor:
    @staticmethod
    def load(path):
        img = mpimg.imread(path)
        plt.imshow(img)
        print("Loading image of dimensions {} x {}".format(*img.shape))
        return img

    @staticmethod
    def display(array):
        img = plt.imshow(array, cmap='gray')
        plt.show(block=True)
