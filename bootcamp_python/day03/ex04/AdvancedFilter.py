import numpy as np

class AdvancedFilter:
    @staticmethod
    def pad_array(array, pad):
        padded_arr = np.zeros((array.shape[0] + 2*pad, array.shape[1] + 2*pad, array.shape[2]))
        padded_arr[pad:-pad,pad:-pad,:] = array
        return padded_arr

    # https://github.com/ashushekar/image-convolution-from-scratch
    @staticmethod
    def apply_kernel(array, kernel, size):
        padded_arr = AdvancedFilter.pad_array(array, int(size / 2))
        output_arr = np.empty(array.shape)
        for z in range(array.shape[2]):
            for y in range(array.shape[1]):
                for x in range(array.shape[0]):
                    output_arr[x, y, z] = (kernel * padded_arr[x:x+size,y:y+size,z:z+1]).sum() / kernel.sum()
        return output_arr

    @staticmethod
    def mean_blur(array, size=3):
        m_kernel = np.ones((size, size))
        output_arr = AdvancedFilter.apply_kernel(array, m_kernel, size)
        return output_arr
    
    # https://stackoverflow.com/questions/29731726/how-to-calculate-a-gaussian-kernel-matrix-efficiently-in-numpy
    @staticmethod
    def gen_gkernel(size=3, sig=1):
        ax = np.linspace(-(size-1) / 2., (size-1) / 2., size)
        xx, yy = np.meshgrid(ax, ax)
        kernel = np.exp(-0.5*(np.square(xx)+np.square(yy))/np.square(sig))
        return kernel

    @staticmethod
    def gaussian_blur(array, size=3):
        g_kernel = AdvancedFilter.gen_gkernel(size)
        output_arr = AdvancedFilter.apply_kernel(array, g_kernel, size)
        return output_arr
