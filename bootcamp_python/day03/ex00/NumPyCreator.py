"""
Data science is a branch of computer science where we study how to store, use and analyze data for deriving information from it.
"""
import numpy as np

class NumPyCreator:
    @staticmethod
    def from_list(lst, dtype=int):
        return np.array(lst, dtype)

    @staticmethod
    def from_tuple(tpl, dtype=int):
        return np.array(tpl, dtype)

    @staticmethod
    def from_iterable(itr, dtype=int):
        return np.array(itr, dtype)

    @staticmethod
    def from_shape(shape, value=0, dtype=int):
        return np.full(shape, value, dtype)

    @staticmethod
    def random(shape):
        return np.random.rand(*shape)

    @staticmethod
    def identity(n, dtype=int):
        return np.identity(n, dtype)
