import numpy as np

class ScrapBooker:
    @staticmethod
    def crop(array, dimensions, position=(0,0)):
        h1, w1, c1 = array.shape
        h2, w2 = dimensions
        x, y = position
        if h2 > h1 or w2 > w1:
            print('Error: dimensions is larger than the current image size')
            exit(-1)
        crop_arr = array[x: h2 + x, y: w2 + y,:]
        return crop_arr

    @staticmethod
    def thin(array, n, axis):
        h, w, c = array.shape
        if axis == 0:
            return array[:h-n,:,:]
        if axis == 1:
            return array[:,:w-n,:]

    @staticmethod
    def juxtapose(array, n, axis):
        stack_arr = array
        if axis == 0:
            for _ in range(n-1):
                stack_arr = np.vstack((stack_arr, array))
            return stack_arr
        if axis == 1:
            for _ in range(n-1):
                stack_arr = np.hstack((stack_arr, array))
            return stack_arr

    @staticmethod
    def mosaic(array, dimensions):
        h, w = dimensions
        mosaic_arr = ScrapBooker.juxtapose(array, h, 0)
        mosaic_arr = ScrapBooker.juxtapose(mosaic_arr, w, 1)
        return mosaic_arr
