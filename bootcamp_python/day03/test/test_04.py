import sys
import importlib.util
spec = importlib.util.spec_from_file_location('ImageProcessor', '../ex01/ImageProcessor.py')
ImageProcessor = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ImageProcessor)
from AdvancedFilter import AdvancedFilter

if __name__ == '__main__':
    imp = ImageProcessor.ImageProcessor()
    arr = imp.load(sys.argv[1])
    af = AdvancedFilter()
#    imp.display(cf.invert(arr))
#    imp.display(cf.to_blue(arr))
#    imp.display(cf.to_green(arr))
#    imp.display(cf.to_red(arr))
#    imp.display(cf.celluloid(arr))
    imp.display(af.mean_blur(arr))
#    imp.display(af.gaussian_blur(arr))
#    print(af.gen_gkern())

