import sys
import importlib.util
spec = importlib.util.spec_from_file_location('ImageProcessor', '../ex01/ImageProcessor.py')
ImageProcessor = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ImageProcessor)
from ColorFilter import ColorFilter

if __name__ == '__main__':
    imp = ImageProcessor.ImageProcessor()
    arr = imp.load(sys.argv[1])
    cf = ColorFilter()
#    imp.display(cf.invert(arr))
#    imp.display(cf.to_blue(arr))
#    imp.display(cf.to_green(arr))
#    imp.display(cf.to_red(arr))
#    imp.display(cf.celluloid(arr))
    imp.display(cf.to_grayscale(arr))
