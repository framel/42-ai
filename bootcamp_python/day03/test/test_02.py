import sys
import importlib.util
spec = importlib.util.spec_from_file_location('ImageProcessor', '../ex01/ImageProcessor.py')
ImageProcessor = importlib.util.module_from_spec(spec)
spec.loader.exec_module(ImageProcessor)
from ScrapBooker import ScrapBooker 

if __name__ == '__main__':
    imp = ImageProcessor.ImageProcessor()
    arr = imp.load(sys.argv[1])
    scr = ScrapBooker()
#    imp.display(scr.crop(arr, (100, 200), (50, 100)))
#    imp.display(scr.thin(arr, 50, 1))
#    imp.display(scr.juxtapose(arr, 2, 1))
    imp.display(scr.mosaic(arr, (4, 6)))
