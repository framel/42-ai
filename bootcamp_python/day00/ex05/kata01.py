if __name__ == '__main__':
    languages = {
        'Python': 'Guido van Rossum',
        'Ruby': 'Yukihiro Matsumoto',
        'PHP': 'Rasmus Lerdorf',
    }
    keys = list(languages.keys())
    for key in keys:
        print(f'{key} was created by {languages[key]}')
