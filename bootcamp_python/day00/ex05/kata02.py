if __name__ == '__main__':
    values = (3,30,2019,9,25)
    keys = ('hour', 'minutes', 'year', 'month', 'day')
    date = dict(zip(keys, values))
    print('{month}/{day}/{year} {hour}:{minutes}'.format(**date))
