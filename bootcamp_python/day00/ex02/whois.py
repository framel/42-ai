import sys

def whois(num):
    if num == 0:
        return("I'm Zero.")
    elif num % 2:
        return("I'm Odd.")
    else:
        return("I'm Even.")

if __name__ == '__main__':
    length = len(sys.argv)
    if length == 1:
        pass
    elif length == 2 and sys.argv[1].isnumeric():
        print(whois(int(sys.argv[1])))
    else:
        print("ERROR")
