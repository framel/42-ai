import sys

def operations_help():
        usage = """Usage: python operations.py <number1> <number2>
Example:
    python operations.py 10 3
"""
        print(usage)

def quotient(num1, num2):
    if num2 == 0:
        return ("ERROR (div by zero)")
    return(num1 / num2)

def remainder(num1, num2):
    if num2 == 0:
        return ("ERROR (modulo by zero)")
    return(num1 % num2)

def operations(num1, num2):
    print(f'Sum:\t\t{num1 + num2}')
    print(f'Difference:\t{num1 - num2}')
    print(f'Product:\t{num1 * num2}')
    print(f'Quotient:\t{quotient(num1, num2)}')
    print(f'Remainder:\t{remainder(num1, num2)}\n')
        
if __name__ == '__main__':
    n_args = len(sys.argv)
    if n_args == 1 or n_args == 2:
        operations_help()
    elif n_args == 3:
        if sys.argv[1].isnumeric() and sys.argv[2].isnumeric():
            operations(int(sys.argv[1]), int(sys.argv[2]))
        else:
            print("InputError: only numbers\n")
            operations_help()
    else:
        print("InputError: too many arguments\n")
        operations_help()
