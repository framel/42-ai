import sys

morse_code = {'a':'.-',
              'b':'-...',
              'c':'-.-.',
              'd':'-..',
              'e':'.',
              'f':'..-.',
              'g':'--.',
              'h':'....',
              'i':'..',
              'j':'.---',
              'k':'-.-',
              'l':'.-..',
              'm':'--',
              'n':'-.',
              'o':'---',
              'p':'.--.',
              'q':'--.-',
              'r':'.-.',
              's':'...',
              't':'-',
              'u':'..-',
              'v':'...-',
              'w':'.--',
              'x':'-..-',
              'y':'-.--',
              'z':'--..',
              '0':'-----',
              '1':'.----',
              '2':'..---',
              '3':'...--',
              '4':'....-',
              '5':'.....',
              '6':'-....',
              '7':'--...',
              '8':'---..',
              '9':'----.',
              ' ':'/'}

def sos(message):
    if not message:
        return
    message = ' '.join(message)
    message = message.lower()
    code = ''
    for letter in message:
        if letter in morse_code.keys():
            code += morse_code.get(letter)
            code += ' '
        else:
            print('ERROR')
            return
    print(code)
            
if __name__ == '__main__':
    sos(sys.argv[1::])

