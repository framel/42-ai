import sys

def filterwords(phrase, n):
    phrase = list(phrase)
    for letter in phrase:
        if not letter.isalnum() and not letter.isspace():
            phrase.remove(letter)
    cleaned = ""
    for letter in phrase:
        cleaned += letter
    phrase = cleaned.split(' ')
    answer = phrase.copy()
    for word in phrase:
        if len(word) <= n:
            answer.remove(word)
    print(answer)

if __name__ == '__main__':
    if len(sys.argv) == 3 and not sys.argv[1].isnumeric()\
       and sys.argv[2].isnumeric():
        filterwords(sys.argv[1], int(sys.argv[2]))
    else:
        print('ERROR')
