def text_analyzer(text=None):
    """This function counts the number of upper characters, \
lower characters, punctuation and spaces in a given text."""

    if text == None:
        text = input("What is the text to analyse?\n")
    length = len(text)
    upper = 0
    lower = 0
    space = 0
    punc = 0
    for letter in text:
        if letter.isupper():
            upper += 1
        elif letter.islower():
            lower += 1
        elif letter.isspace():
            space += 1
        elif letter.isdigit():
            pass
        else:
            punc += 1
    print(f'The text contains {length} characters:\n')
    print(f'- {upper} upper letters\n')
    print(f'- {lower} lower letters\n')
    print(f'- {punc} punctuation marks\n')
    print(f'- {space} spaces')
