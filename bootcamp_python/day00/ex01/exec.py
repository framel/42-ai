import sys

def concat_args(args):
    args.pop(0)
    return(' '.join(args))

if __name__ == '__main__':
    args = concat_args(sys.argv)
    args = args.swapcase()
    args = args[::-1]
    print(args)
