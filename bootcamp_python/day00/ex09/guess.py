from random import randint

def compare(num1, num2):
    diff = num1 - num2
    if diff < 0:
        print("Too low!")
    elif diff > 0:
        print("Too high!")
    return(diff)

def guess():
    answer = ''
    trails = 0
    secret = randint(0, 99)
    while answer != 'exit':
        print("What's your guess between 1 and 99?")
        answer = input('>> ')
        trails += 1
        if answer.isnumeric():
            if not compare(int(answer), secret):
                if secret == 42:
                    print("The answer to the ultimate question of life, the universe and everything is 42.")
                return (trails)
        elif answer != 'exit':
            print("That's not a number.")
    print('Goodbye')

if __name__ == '__main__':
    message = """This is an interactive guessing game!
You have to enter a number between 1 and 99 to find out the secret number.
Type 'exit' to end the game.
Good luck!
"""
    print(message)
    trails = guess()
    if trails == 1:
        print("Congratulations! You got it on your first try!")
    elif trails:
        print("Congratulations, you've got it!")
        print(f"You won in {trails} attempts!")
