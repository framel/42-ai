cookbook = {'sandwich':{'ingredients':['ham',
                                       'bread',
                                       'cheese',
                                       'tomatoes'],
                        'meal':'lunch',
                        'prep_time':10},
            'cake':{'ingredients':['flour',
                                   'sugar',
                                   'eggs'],
                    'meal':'dessert',
                    'prep_time':60},
            'salad':{'ingredients':['avocado',
                                    'arugula',
                                    'tomatoes',
                                    'spinach'],
                     'meal':'lunch',
                     'prep_time':15}}

def print_menu():
    print("""Please select an option\
 by typing the corresponding number:
1: Add a recipe
2: Delete a recipe
3: Print a recipe
4: Print the cookbook
5: Quit""")

def get_input():
    answer = input('>> ')
    print()
    return (answer)

def add_recipe():
    print('How would you name the recipe?')
    name = get_input()
    if not name:
        print('Recipe must be named!\n')
        print_menu()
        return
    for recipe in cookbook:
        if name == recipe:
            print(f'The recipe of {name} is already the cookbook.\n')
            print_menu()
            return
    print(f'Which ingredients would {name} contains?')
    print('One per line. Hit ENTER key when finish.')
    ingredient = True
    ingr_list = []
    while(ingredient):
        ingredient = input('>> ')
        if ingredient:
            ingr_list.append(ingredient)
    print(ingr_list)
    print(f'What type of meal {name} is?')
    meal_type = get_input()
    print(f'How many minutes to cook {name}?')
    time = get_input()
    if not time.isnumeric():
        print(f'Preparation name must be a number.\n')
        print_menu()
        return
    cookbook[name] = {'ingredients':ingr_list,
                      'meal':meal_type,
                      'prep_time':time}
    print(f'The recipe of {name} is record in the cookbook\n')

def choose_recipe(message):
    print(message)
    choice = get_input()
    for recipe in cookbook:
        if choice == recipe:
            return(recipe)
    print('This recipe is not in the cookbook.\n')
    print_menu()

def del_recipe():
    recipe = choose_recipe("Which recipe would you delete ?")
    if recipe:
        cookbook.pop(recipe)

def print_recipe(recipe=None):
    if not recipe:
        recipe = choose_recipe("Please enter the recipe's name to get its details:")
    if recipe:
        print(f'Recipe for {recipe}:')
        print(f'Ingredients list: {cookbook[recipe]["ingredients"]}')
        print(f'To be eaten for {cookbook[recipe]["meal"]}.')
        print(f'Takes {cookbook[recipe]["prep_time"]} minutes of cooking.')

def print_cookbook():
    for recipe in cookbook:
        print_recipe(recipe)
        print()

def select_opt():
    option = get_input()
    if option == '1':
        add_recipe()
    elif option == '2':
        del_recipe()
    elif option == '3':
        print_recipe()
    elif option == '4':
        print_cookbook()
    elif option == '5':
        print('Cookbook closed.')
    else:
        print("""This option does not exist, please type the corresponding number.
To exit, enter 5.""")
    return (option)

if __name__ == '__main__':
    option = 0
    print_menu()
    while option != '5':
        option = select_opt()
