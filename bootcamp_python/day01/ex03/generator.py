from time import time

def gen_number(maximum):
    return int(time() * 1000) % maximum

def generator(text, sep=" ", option=None):
    '''Option is an optional arg, sep is mandatory'''
    if type(text) is not str:
        print('ERROR')
        return
    if option:
        if option not in ['shuffle','unique','ordered']:
            print('ERROR')
            return
    gen = text.split(sep)
    if option == 'shuffle':
        new_list = []
        length = len(gen)
        while length:
            new_list.append(gen.pop(gen_number(length)))
            length -= 1
        gen = new_list
    elif option == 'unique':
        new_list = []
        for elem in gen:
            if elem not in new_list:
                new_list.append(elem)
            gen = new_list
    elif option == 'ordered':
        gen = sorted(gen, key=str.lower)
    for word in gen:
        yield word
