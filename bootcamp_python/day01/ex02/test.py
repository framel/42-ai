from vector import Vector

v1 = Vector([0.0, 1.0, 2.0, 3.0])
v1bis = Vector([1.0, 1.0, 2.0, 4.0])
print(v1)
#print(Vector([0.0, 1.0, 2.0, 3.0]).size)
print(repr(v1))
v2 = v1 * 5
#print(v2)
v2r = 5 * v1
#print(v2r)
v2v2 = v1 * v1bis
#print(v2v2)
v3 = Vector(3)
print(str(v3.values))
v4 = Vector((10,15))
#print("v4.values =", v4.values)
#print("v4.size =", v4.size)
#print(v1 + 5)
#print(4 + v1)
#print(v1 - 5)
#print(5 - v1)
#print(5 / v1bis)
#print(v1bis / 5)
