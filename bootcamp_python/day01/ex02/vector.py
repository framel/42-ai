def float_range(start, end, step=1):
    num_list = []
    for i in range(start, end, step):
        num_list.append(float(i))
    return num_list

class Vector:
    def __init__(self, arg):
        #TODO: Error handling
        if type(arg) is list:
            self.values = arg
            self.size = len(arg)
        elif type(arg) is int:
            self.values = float_range(0, arg)
            self.size = arg
        elif type(arg) is tuple:
            self.values = float_range(arg[0], arg[1])
            self.size = arg[1] - arg[0]

    def __add__(self, other):
        new_values = []
        for elem in self.values:
            new_values.append(elem + other)
        return new_values

    def __radd__(self, other):
        new_values = []
        for elem in self.values:
            new_values.append(elem + other)
        return new_values

    def __sub__(self, other):
        new_values = []
        for elem in self.values:
            new_values.append(elem - other)
        return new_values

    def __rsub__(self, other):
        new_values = []
        for elem in self.values:
            new_values.append(elem - other)
        return new_values

    def __truediv__(self, other):
        new_values = []
        for elem in self.values:
            new_values.append(elem / other)
        return new_values

    def __rtruediv__(self, other):
        new_values = []
        for elem in self.values:
            new_values.append(elem / other)
        return new_values

    def __mul__(self, other):
        if isinstance(other, Vector):
            x = self.values
            y = other.values
            if len(x) != len(y):
                print("{}: Vectors must be equal-length".format(__class__.__name__))
                exit(-1)
            return sum(x_i*y_i for x_i, y_i in zip(x, y))
        new_values = []
        for elem in self.values:
            new_values.append(elem * other)
        return new_values

    def __rmul__(self, other):
        new_values = []
        for elem in self.values:
            new_values.append(other * elem)
        return new_values

    def __str__(self):
        txt = ''
        txt += str(self.values)
        return txt

    def __repr__(self):
        txt = "{'values':"
        txt += str(self.values)
        txt += ",'size':" + str(self.size) + "}"
        return txt
