class Account(object):

    ID_COUNT = 1

    def __init__(self, name, **kwargs):
        self.id = self.ID_COUNT
        self.name = name
        self.__dict__.update(kwargs)
        if hasattr(self, 'value'):
            self.value = 0
        Account.ID_COUNT += 1

    def transfer(self, amount):
        self.value += amount

def attr_startswith(attributes, start):
    for elem in attributes:
        if elem.startswith(start):
            return True
    return False

def iscorrupt(account):
    attributes = dir(account)
    if not len(attributes) % 2:
        return True
    for attr in attributes:
        if attr[0] == 'b':
            return True
    if not attr_startswith(attributes, 'zip') \
       and not attr_startswith(attributes, 'addr'):
        return True
    if not hasattr(account, 'name') \
       or not hasattr(account, 'id') \
       or not hasattr(account, 'value'):
        return True
    return False
        
class Bank(object):
    """The bank"""
    def __init__(self):
        self.account = []

    def add(self, account):
        self.account.append(account)

    def transfer(self, origin, dest, amount):
        """
            @origin:  int(id) or str(name) of the first account
            @dest:    int(id) or str(name) of the destination account
            @amount:  float(amount) amount to transfer
            @return         True if success, False if an error occured
        """
        if type(origin) is str:
            for elem in self.account:
                if getattr(elem, 'name') == origin:
                    origin = elem
        elif origin <= len(self.account):
            origin = self.account[origin - 1]
        if not isinstance(origin, Account):
            return False
        if iscorrupt(origin):
            if not Bank.fix_account(self, origin.id):
                return False
        if type(dest) is str:
            for elem in self.account:
                if getattr(elem, 'name') == dest:
                    dest = elem
        elif dest <= len(self.account):
            dest = self.account[dest - 1]
        if not isinstance(dest, Account):
            return False
        if iscorrupt(dest):
            if not Bank.fix_account(self, dest.id):
                return False
        if amount < 0 or origin.value < amount:
            return False
        origin.transfer(-amount)
        dest.transfer(amount)
        return True

    def fix_account(self, account):
        """
            fix the corrupted account
            @account: int(id) or str(name) of the account
            @return         True if success, False if an error occured
        """
        if type(account) is str:
            for elem in self.account:
                if getattr(elem, 'name') == account:
                    account = getattr(elem, 'id')                    
            if type(account) is str:
                return False
        print(len(self.account))
        current = self.account[account - 1]
        attributes = dir(current)
        if not len(attributes) % 2:
            setattr(current, 'fix_attr', True)
        for attr in attributes:
            if attr[0] == 'b':
                delattr(current, attr)
        if not attr_startswith(attributes, 'zip') \
           and not attr_startswith(attributes, 'addr'):
            setattr(current, 'zip', 42420)
        if not hasattr(current, 'name'):
            setattr(current, 'name', 'account_' + account)
        if not hasattr(current, 'value'):
            setattr(current, 'value', 0)
        if not hasattr(current, 'id') or iscorrupt(current):
            return False
        return True

