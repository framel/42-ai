from datetime import datetime
from recipe import Recipe

class Book:
    def __init__(self, name, last_update, creation_date, recipes_list):
        if type(name) is not str:
            print("Class {}: name must be a string"
                .format(__class__.__name__))
            exit(-1)
        if not isinstance(last_update, datetime):
            print("Class {}: last_update must be an instance of datetime"
                .format(__class__.__name__))
            exit(-1)
        if not isinstance(creation_date, datetime):
            print("Class {}: creation_date must be an instance of datetime"
                .format(__class__.__name__))
            exit(-1)
        if type(recipes_list) is not dict:
            print("Class {}: recipes_list must be a dict"
                .format(__class__.__name__))
            exit(-1)
        elif len(recipes_list) != 3 \
             or 'starter' not in recipes_list \
             or 'lunch' not in recipes_list \
             or 'dessert' not in recipes_list:
            print("Class {}: recipes_list keys are 'starter', 'lunch' and 'dessert'"
                .format(__class__.__name__))
            exit(-1)
        self.name = name
        self.last_update = last_update
        self.creation_date = creation_date
        self.recipes_list = recipes_list

    def get_recipe_by_name(self, name):
        """Print a recipe with the name `name` and return the instance"""
        for key in self.recipes_list:
            for elem in self.recipes_list[key]:
                if getattr(elem, 'name') == name:
                    print(elem)

    def get_recipes_by_types(self, recipe_type):
        """Get all recipe names for a given recipe_type """
        if recipe_type in ['starter', 'lunch', 'dessert']:
            for elem in self.recipes_list[recipe_type]:
                print(elem)
        else:
            print("{}: recipe_type must be 'starter', 'lunch' or 'dessert'".format(__class__.__name__))

    def add_recipe(self, recipe):
        """Add a recipe to the book and update last_update"""
        if not isinstance(recipe, Recipe):
            print("Class {}: recipe must be an instance of Recipe"
                .format(__class__.__name__))
            exit(-1)
        self.recipes_list[recipe.recipe_type].append(recipe)
        self.last_update = datetime.now()
