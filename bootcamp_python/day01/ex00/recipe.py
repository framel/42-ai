class Recipe:
    def __init__(self, name, cooking_lvl, cooking_time,
                 ingredients, recipe_type, description=''):
        if type(name) is not str:
            print("Class {}: name must be a string"
                .format(__class__.__name__))
            exit(-1)
        if type(cooking_lvl) is not int or int(cooking_lvl) \
           not in range(1,6):
            print("Class {}: cooking_lvl must be a integer in range of 1 to 5"
                  .format(__class__.__name__))
            exit(-1)
        if type(cooking_time) is not int:
            print("Class {}: cooking_time must be an integer"
                .format(__class__.__name__))
            exit(-1)
        if type(ingredients) is not list:
            print("Class {}: ingredients must be a list"
                .format(__class__.__name__))
            exit(-1)
        if recipe_type not in ["starter","lunch","dessert"]:
            print("Class {}: recipe_type must be 'starter', 'lunch' or 'dessert'"
                .format(__class__.__name__))
            exit(-1)
        if type(description) is not str:
            print("Class {}: description must be a string"
                .format(__class__.__name__))
            exit(-1)
        self.name = name
        self.cooking_lvl = cooking_lvl
        self.cooking_time = cooking_time
        self.ingredients = ingredients
        self.recipe_type = recipe_type
        self.description = description

    def __str__(self):
        """Return the string to print with the recipe info"""
        txt = ""
        txt += "{'name':'" + self.name + "',"
        txt += "'cooking_lvl':" + str(self.cooking_lvl) + ",\n"
        txt += "'cooking_time':" + str(self.cooking_time) + ",\n"
        txt += "'ingredients':" + str(self.ingredients) + ",\n"
        txt += "'recipe_type':'" + self.recipe_type + "',\n"
        txt += "'description':'" + self.description + "'}"
        return txt
