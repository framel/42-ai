from book import Book
from recipe import Recipe
from datetime import datetime

tourte = Recipe('tourte', 2, 30, ['flour', 'eggs', 'mushrooms', 'pepper'], 'starter', 'A french pie')
salad = Recipe('salad', 1, 10, ['salad', 'tomatoes', 'mushrooms', 'pepper'], 'starter', 'A simple salad')
crepe = Recipe('crepe', 1, 25, ['flour', 'eggs', 'milk', 'sugar'], 'dessert', 'A french pancake')
to_print = str(tourte)
print(to_print)
livre = Book("Livre", datetime.now(), datetime.now(), {'starter':[], 'lunch': [], 'dessert': []})
livre.add_recipe(tourte)
livre.add_recipe(crepe)
livre.add_recipe(salad)
livre.get_recipe_by_name('crepe')
livre.get_recipes_by_types('starter')
