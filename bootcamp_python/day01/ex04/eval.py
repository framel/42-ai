class Evaluator:
    @staticmethod
    def zip_evaluate(coefs, words):
        if len(coefs) != len(words):
            return -1
        lengths = []
        for word in words:
            lengths.append(len(word))
        zipped = zip(coefs, lengths)
        accu = 0
        for coef,word in zipped:
            accu += coef * word
        print(accu)

    @staticmethod
    def enumerate_evaluate(coefs, words):
        if len(coefs) != len(words):
            return -1
        accu = 0
        for word in words:
            enum = enumerate(word)
            for letter in enum:
                accu += coefs[0]
            coefs.pop(0)
        print(accu)
